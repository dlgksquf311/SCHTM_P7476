
# import packages

import GetOldTweets3 as got
from bs4 import BeautifulSoup
import datetime
import time
from random import uniform
import pandas as pd


re_path = './data/twitter'


def main():

	days_range = []
	tweet_list = []
	'''
	date = input('수집 시작 날짜를 YYYY-mm-dd 형태로 입력해주세요')
	date_generated = set_date(date)

	for date in date_generated:
		days_range.append(date.strftime('%Y-%m-%d'))
	'''
	food = ['사과파이','케이크','커피']
	
	for keyword in food:
		tweet_list = crawling_twitter(tweet_list,keyword)
		save_csv(tweet_list,keyword)

		

'''
def set_date(date):
	start = datetime.datetime.strptime(date,'%Y-%m-%d')
	end = datetime.datetime.today()
	date_generated = [start+datetime.timedelta(days=x) for x in range(0,(end-start).days)]
	return date_generated

'''
# 해당 음식이 언급된 트위터 가져오기
def crawling_twitter(tweet_list,keyword):
	
	#global start_date
	#global end_date2
	#global keyword
	global name

	#start_date = days_range[0]
	#end_date = (datetime.datetime.strptime(days_range[-1],"%Y-%m-%d") + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
	# date_generated = [start+datetime.timedelta(days=x) for x in range(0,(end-start).days)]
	
	TwitterCri = got.manager.TweetCriteria().setQuerySearch(keyword)\
			.setMaxTweets(-1)

	tweet = got.manager.TweetManager.getTweets(TwitterCri)

	for index in tweet:
		username = index.username
		link = index.permalink
		content = index.text
#		tweet_date = index.date.strftime('%Y-%m-%d')
#		tweet_time = index.date.strftime('%H:%M:%S')
		retweets = index.retweets
		favorites = index.favorites

		info_list = [username,content,link,retweets,favorites]
		tweet_list.append(info_list)
		
		time.sleep(uniform(1,2))

	return tweet_list


def save_csv(tweet_list,keyword,start_date,end_date):
	
	twitter_df = pd.DataFrame(tweet_list,columns = ['user_name','text','link','retweet_counts','favorite_counts'])
	twitter_df.to_csv('./data/twitter/twitter_food_{}_{}_to_{}.csv'.format(keyword,start_date,end_date))
	

if  __name__ == '__main__':
	main()	




