from bs4 import BeautifulSoup as bs
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# chrome driver 열기
def init_driver():
     driver = webdriver.Chrome('E://chromedriver.exe')
     driver.wait = WebDriverWait(driver, 5)
     return driver

# chrome driver 닫기
def close_driver(driver):
    driver.close()
    return

# twitter login
def login_twitter(driver, username, password):
    
    # id, pw 입력 위치 지정
    driver.get("https://twitter.com/login")   
    driver.wait = WebDriverWait(driver,20)
    username_field = driver.find_element(By.NAME,'session[username_or_email]')
    password_field = driver.find_element(By.NAME,'session[password]')
    
    # id, pw 입력
    username_field.send_keys(username)
    driver.implicitly_wait(1)
    password_field.send_keys(password)
    driver.implicitly_wait(15)
    button = driver.find_element_by_class_name("css-901oao.css-16my406.css-bfa6kz.r-1qd0xha.r-ad9z0x.r-bcqeeo.r-qvutc0")
    button.click()
        
# 트위터 크롤링    
def twicralwer(driver,query):
    # 검색창 위치 지정 및 입력
    box = driver.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[data-testid='SearchBox_Search_Input'][role='combobox']")))
    driver.find_element(By.CSS_SELECTOR, "input[data-testid='SearchBox_Search_Input']")
    box.send_keys(query)
    box.submit() 
    
    # 크롤링
    page_sources = []
    past_page = []
    times = 0
    
    while True:    
        if times % 300 == 0: 
            past_page = page_sources           
        tweets = driver.find_elements(By.CSS_SELECTOR,"div[data-testid='tweet']")
        try:
		driver.execute_script("arguments[0].scrollIntoView();",tweets[-1])
	except:
		break
        page_sources.append(driver.page_source)
        times += 1
        time.sleep(1)
        
        # 데이터가 2만개 이상이거나, 스크롤하여 더이상 크롤링할 게시글이 없으면 스크롤 종료
        if len(page_sources) > 20000:
            break                        
        elif (times > 300) & (len(past_page) == len(page_sources)) :
            break
    return page_sources

# 크롤링한 html 에서 정보 추출
def extract_tweets(page_sources,word):
    tweets = []
    for page_source in page_sources:
        soup = bs(page_source,'lxml')
        for li in soup.find_all("article"):
       
            tweet = {
                'text':None,
                'keyword':None
                }
            
            texts = li.find_all('span')
            if texts is not None:
                full_text = texts[0].get_text()
                for content in texts[1:len(texts)]:
                    content = content.get_text()
                    full_text = full_text + content
                
            tweet['text'] = full_text
            tweet['keyword'] = word 
            tweets.append(tweet)
    return tweets

# 크롤링한 데이터를 데이터프레임으로 전환 후 csv로 저장
def save_csv(tweets):
    twitter_crawling_result =  pd.DataFrame(tweets)
    twitter_crawling_result.to_csv('E:\\textmining\\data\\트위터크롤링.csv',encoding='utf-8-sig')

# main함         
def main():
    driver = init_driver()
    username = 'dlguswjd0226'
    password = 'snoho0226!'
    login_twitter(driver,username,password)
   
    words = ['떡볶이','피자','스파게티','집밥','술','아메리카노','초콜릿','아이스크림']
    result = list()
    
    for word in words:
        homebutton = driver.find_element(By.CSS_SELECTOR,"a[href='/home'][role='button']")
        homebutton.click()
        page_source = twicralwer(driver,word)
        tweets = extract_tweets(page_source,word)
        result.append(tweets)
           
    close_driver(driver)
    save_csv(result)

    
if __name__=='__main__':
    main()
    
    
    
